import currency from 'currency.js'

const RUPIAH_FORMAT = value => currency(value, {
  symbol: 'Rp'
})

export function rupiah (value) {
  return RUPIAH_FORMAT(value).format(true)
}
