export function authorName (author) {
  return `${author.title} ${author.firstName}`
}
