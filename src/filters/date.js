import dayjs from 'dayjs'

export function date (value) {
  return dayjs(value).format('DD-MM-YYYY HH:mm')
}
