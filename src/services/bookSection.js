import http from './http'

export default {
  async create (payload) {
    try {
      const res = await http.post('/v1/cs/book-sections', payload)

      return res.data.data
    } catch (error) {
      throw new Error(error)
    }
  },

  async find (id) {
    try {
      const res = await http.get(`/v1/cs/book-sections/${id}`)

      return res.data.data
    } catch (error) {
      throw new Error(error)
    }
  },

  async update (id, payload) {
    try {
      const res = await http.patch(`/v1/cs/book-sections/${id}`, payload)

      return res.data.data
    } catch (error) {
      throw new Error(error)
    }
  }
}
