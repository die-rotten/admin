import http from './http'

export default {
  async upload (payload) {
    const formData = new FormData()

    formData.append('file', payload)

    try {
      const res = await http.post('/v1/upload', formData)

      return res.data
    } catch (error) {
      throw new Error(error)
    }
  }
}
