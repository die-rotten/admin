import http from './http'
import { get, set } from '@/plugins/cookie'
import { ACCESS_TOKEN_KEY } from '@/shared'

export default {
  async authenticate ({ email, password }) {
    try {
      const response = await http.post('/v1/auth', {
        username: email,
        password: password,
        grant_type: 'password',
        client_secret: process.env.VUE_APP_CLIENT_SECRET,
        client_id: process.env.VUE_APP_CLIENT_ID
      })

      http.setHeader(response.data.access_token)
      set(ACCESS_TOKEN_KEY, response.data.access_token)

      return response.data.access_token
    } catch (error) {
      throw new Error(error)
    }
  },

  async whoami () {
    try {
      const token = get(ACCESS_TOKEN_KEY)
      http.setHeader(token)
      const response = await http.get('/v1/whoami')

      return response.data.data
    } catch (error) {
      throw new Error(error)
    }
  }
}
