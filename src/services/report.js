import http from './http'

export default {
  async getAuthorsAmount () {
    try {
      const res = await http.get('/v1/cs/reports/authors-amount')

      return res.data.data
    } catch (error) {
      throw new Error(error)
    }
  },

  async getPackagesAmount () {
    try {
      const res = await http.get('/v1/cs/reports/packages-amount')

      return res.data.data
    } catch (error) {
      throw new Error(error)
    }
  },

  async getTransactionsQuantity () {
    try {
      const res = await http.get('/v1/ts/reports/transactions-quantity')

      return res.data.data
    } catch (error) {
      throw new Error(error)
    }
  },

  async getUsersAmount () {
    try {
      const res = await http.get('/v1/us/reports/customers')

      return res.data
    } catch (error) {
      throw new Error(error)
    }
  },

  async getPopularPackages () {
    try {
      const res = await http.get('/v1/cs/reports/popular-packages')

      return res.data.data
    } catch (error) {
      throw new Error(error)
    }
  },

  async getTransactionsAmount (year = 2020) {
    try {
      const res = await http.get(`/v1/ts/reports/transactions-amount?year=${year}`)

      console.log(res)
      return res.data.data
    } catch (error) {
      throw new Error(error)
    }
  }
}
