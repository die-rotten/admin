import axios from 'axios'

export default {
  init () {
    axios.defaults.baseURL = process.env.VUE_APP_BASE_URL
  },

  setHeader (token) {
    axios.defaults.headers.Authorization = `Bearer ${token}`
  },

  getHeader () {
    return axios.defaults.headers.Authorization
  },

  async get (url) {
    return await axios.get(url)
  },

  async post (url, data) {
    return await axios.request({
      method: 'post',
      url,
      data
    })
  },

  async patch (url, data) {
    return await axios.patch(url, data)
  },

  async delete (url) {
    return await axios.delete(url)
  }
}
