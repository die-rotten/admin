import http from './http'

export default {
  async get () {
    try {
      const res = await http.get('/v1/cs/packages')

      return res.data.data
    } catch (error) {
      throw new Error(error.message)
    }
  },

  async find (id) {
    try {
      const res = await http.get(`/v1/cs/packages/${id}`)

      return res.data.data
    } catch (error) {
      throw new Error(error.message)
    }
  },

  async create (payload) {
    try {
      const res = await http.post('/v1/cs/packages', payload)

      return res.data.data
    } catch (error) {
      throw new Error(error.message)
    }
  },

  async update (id, payload) {
    try {
      const res = await http.patch(`/v1/cs/packages/${id}`, payload)

      return res.data.data
    } catch (error) {
      throw new Error(error.message)
    }
  }
}
