import http from './http'

export default {
  async get () {
    try {
      const res = await http.get('/v1/cs/books')

      return res.data.data
    } catch (error) {
      throw new Error(error)
    }
  },

  async getSections (id) {
    try {
      const res = await http.get(`/v1/cs/books/${id}/sections`)

      return res.data.data
    } catch (error) {
      throw new Error(error)
    }
  },

  async find (id) {
    try {
      const res = await http.get(`/v1/cs/books/${id}`)

      return res.data.data
    } catch (error) {
      throw new Error(error)
    }
  },

  async store (payload) {
    try {
      const res = await http.post('/v1/cs/books', payload)

      return res.data.data
    } catch (error) {
      throw new Error(error)
    }
  },

  async update (id, payload) {
    try {
      const res = await http.patch(`/v1/cs/books/${id}`, payload)

      return res.data.data
    } catch (error) {
      throw new Error(error)
    }
  }
}
