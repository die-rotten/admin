import http from './http'

export default {
  async get () {
    try {
      const res = await http.get('/v1/cs/authors')

      return res.data.data
    } catch (error) {
      throw new Error(error)
    }
  },

  async find (id) {
    try {
      const res = await http.get(`/v1/cs/authors/${id}`)

      return res.data.data
    } catch (error) {
      throw new Error(error)
    }
  },

  async create (payload) {
    try {
      const res = await http.post('/v1/cs/authors', payload)

      return res.data.data
    } catch (error) {
      if (error.response.data.email) throw new Error('Email sudah terdaftar')

      throw new Error(error.response)
    }
  },

  async update (id, payload) {
    try {
      const res = await http.patch(`/v1/cs/authors/${id}`, payload)

      return res.data.data
    } catch (error) {
      throw new Error(error)
    }
  },

  async delete (id) {
    try {
      const res = await http.delete(`/v1/cs/authors/${id}`)

      return res
    } catch (error) {
      throw new Error(error)
    }
  }
}
