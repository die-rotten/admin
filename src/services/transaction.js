import http from './http'

export default {
  async get () {
    try {
      const res = await http.get('/v1/ts/transactions')

      return res.data.data
    } catch (error) {
      throw new Error(error.message)
    }
  },

  async find (id) {
    try {
      const res = await http.get(`/v1/ts/transactions/${id}`)

      return res.data.data
    } catch (error) {
      throw new Error(error.message)
    }
  },

  async findPayment (id) {
    try {
      const res = await http.get(`/v1/ts/confirmations/${id}`)

      return res.data.data
    } catch (error) {
      throw new Error(error.message)
    }
  },

  async approvePayment (id) {
    try {
      const res = await http.patch(`/v1/ts/confirmations/${id}/approve`)

      return res.data.data
    } catch (error) {
      throw new Error(error.message)
    }
  }
}
