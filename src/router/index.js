import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Login from '../views/Login.vue'

import middleware from '@/middlewares/middleware'
import { auth } from '@/middlewares/auth'

Vue.use(VueRouter)

const routes = [
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  {
    path: '/',
    name: 'dashboard',
    component: Home,
    meta: {
      roleIds: [1]
    },
    beforeEnter: middleware([
      auth
    ])
  },
  {
    path: '/publishers',
    name: 'publishers.index',
    component: () => import('@/views/Publisher.vue'),
    meta: {
      roleIds: [1]
    },
    beforeEnter: middleware([
      auth
    ])
  },
  {
    path: '/authors',
    name: 'authors.index',
    component: () => import('@/views/Author.vue'),
    meta: {
      roleIds: [1]
    },
    beforeEnter: middleware([
      auth
    ])
  },
  {
    path: '/books',
    name: 'books.index',
    component: () => import('@/views/Book.vue'),
    meta: {
      roleIds: [1]
    },
    beforeEnter: middleware([
      auth
    ])
  },
  {
    path: '/books/:id',
    name: 'books.details',
    component: () => import('@/views/BookDetail.vue'),
    meta: {
      roleIds: [1]
    },
    beforeEnter: middleware([
      auth
    ])
  },
  {
    path: '/books/:id/contents/create',
    name: 'bookContent.create',
    component: () => import('@/views/BookContentCreate.vue'),
    meta: {
      roleIds: [1]
    },
    beforeEnter: middleware([
      auth
    ])
  },
  {
    path: '/books/:id/contents/:contentId/edit',
    name: 'bookContent.edit',
    component: () => import('@/views/BookContentEdit.vue'),
    meta: {
      roleIds: [1]
    },
    beforeEnter: middleware([
      auth
    ])
  },
  {
    path: '/packages',
    name: 'packages.index',
    component: () => import('@/views/Package.vue'),
    meta: {
      roleIds: [1]
    },
    beforeEnter: middleware([
      auth
    ])
  },
  {
    path: '/packages/:id',
    name: 'packages.detail',
    component: () => import('@/views/PackageDetail.vue'),
    meta: {
      roleIds: [1]
    },
    beforeEnter: middleware([
      auth
    ])
  },
  {
    path: '/transactions',
    name: 'transactions.index',
    component: () => import('@/views/Transaction.vue'),
    meta: {
      roleIds: [1]
    },
    beforeEnter: middleware([
      auth
    ])
  },
  {
    path: '/transactions/:id',
    name: 'transactions.details',
    component: () => import('@/views/TransactionDetail.vue'),
    meta: {
      roleIds: [1]
    },
    beforeEnter: middleware([
      auth
    ])
  }
]

const router = new VueRouter({
  mode: 'history',
  routes
})

export default router
