import book from '@/services/book'

const state = {
  books: [],
  book: {},

  isFetching: false,
  isFetchingError: null,

  isFinding: false,
  isFindingError: null,

  form: {
    title: '',
    description: '',
    authorIds: [],
    price: 0,
    photo: ''
  },

  isCreating: false,
  isCreatingError: null,

  isEditing: false,
  isEditingError: null,

  isUpdating: false,
  isUpdatingError: null
}

const mutations = {
  setFetching (state) {
    state.books = []
    state.isFetching = true
    state.isFetchingError = null
  },

  setFetchingSuccess (state, data) {
    state.books = data
    state.isFetching = false
  },

  setFetchingError (state, error) {
    state.isFetchingError = error
    state.isFetching = false
  },

  setFinding (state) {
    state.books = []
    state.isFinding = true
    state.isFindingError = null
  },

  setFindingSuccess (state, data) {
    state.book = data
    state.isFinding = false
  },

  setFindingError (state, error) {
    state.isFindingError = error
    state.isFinding = false
  },

  setCreating (state) {
    state.isCreating = true
    state.isCreatingError = null
  },

  setCreatingSuccess (state) {
    state.isCreating = false
  },

  setCreatingError (state, error) {
    state.isCreatingError = error
    state.isCreating = false
  },

  setEditing (state) {
    state.isEditing = true
    state.isEditingError = null
  },

  setEditingSuccess (state) {
    state.isEditing = false
  },

  setEditingError (state, error) {
    state.isEditingError = error
    state.isEditing = false
  },

  setUpdating (state) {
    state.isUpdating = true
    state.isUpdatingError = null
  },

  setUpdatingSuccess (state) {
    state.isUpdating = false
  },

  setUpdatingError (state, error) {
    state.isUpdatingError = error
    state.isUpdating = false
  },

  setForm (state, data) {
    const authors = data.authors.data || []
    const authorIds = authors.length > 0 ? authors.map(item => item.id) : []

    state.form = {
      ...data,
      authorIds: authorIds
    }
  },

  resetForm (state) {
    state.form = {
      title: '',
      description: '',
      authorIds: [],
      price: 0,
      photo: ''
    }
  }
}

const actions = {
  async get (context) {
    try {
      context.commit('setFetching')
      const data = await book.get()
      context.commit('setFetchingSuccess', data)
    } catch (error) {
      context.commit('setFetchingError', error)
    }
  },

  async find (context, id) {
    try {
      context.commit('setFinding')
      const data = await book.find(id)
      context.commit('setFindingSuccess', data)
    } catch (error) {
      context.commit('setFindingError', error)
    }
  },

  async create (context, payload) {
    try {
      context.commit('setCreating')
      await book.store(payload)
      context.commit('setCreatingSuccess')
    } catch (error) {
      context.commit('setCreatingError', error)
    }
  },

  async edit (context, id) {
    try {
      context.commit('setEditing')

      const data = await book.find(id)

      context.commit('setForm', data)
      context.commit('setEditingSuccess')
    } catch (error) {
      context.commit('setEditingError', error)
    }
  },

  async update (context, data) {
    const payload = {
      title: data.title,
      description: data.description,
      authorIds: data.authorIds,
      photo: data.photo,
      price: data.price
    }

    try {
      context.commit('setUpdating')
      await book.update(data.id, payload)
      context.commit('setUpdatingSuccess')
    } catch (error) {
      context.commit('setUpdatingError', error)
    }
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
