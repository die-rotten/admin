import bookPackage from '@/services/bookPackage'

const state = {
  packages: [],
  item: {},
  form: {
    id: 0,
    title: '',
    description: '',
    bookIds: [],
    price: 0,
    discount: 0,
    photo: ''
  },

  isFetching: false,
  isFetchingError: null,

  isFinding: false,
  isFindingError: null,

  isCreating: false,
  isCreatingError: null,

  isEditing: false,
  isEditingError: null,

  isUpdating: false,
  isUpdatingError: null
}

const mutations = {
  setFetching (state) {
    state.isFetching = true
    state.isFetchingError = null
  },

  setFetchingSuccess (state, data) {
    state.packages = data
    state.isFetching = false
  },

  setFetchingError (state, error) {
    state.isFetchingError = error
    state.isFetching = false
  },

  setFinding (state) {
    state.isFinding = true
    state.isFindingError = null
  },

  setFindingSuccess (state, data) {
    state.item = data
    state.isFinding = false
  },

  setFindingError (state, error) {
    state.isFindingError = error
    state.isFinding = false
  },

  setCreating (state) {
    state.isCreating = true
    state.isCreatingError = null
  },

  setCreatingSuccess (state) {
    state.isCreating = false
  },

  setCreatingError (state, error) {
    state.isCreating = false
    state.isCreatingError = error
  },

  setEditing (state) {
    state.isEditing = true
    state.isEditingError = null
  },

  setEditingSuccess (state, data) {
    state.form = {
      ...data,
      bookIds: data.books.data
    }
    state.isEditing = false
  },

  setEditingError (state, error) {
    state.isEditing = false
    state.isEditingError = error
  },

  setUpdating (state) {
    state.isUpdating = true
    state.isUpdatingError = null
  },

  setUpdatingSuccess (state) {
    state.isUpdating = false
  },

  setUpdatingError (state, error) {
    state.isUpdating = false
    state.isUpdatingError = error
  },

  resetForm (state) {
    state.form = {
      id: 0,
      title: '',
      description: '',
      bookIds: [],
      price: 0,
      discount: 0,
      photo: ''
    }
  }
}

const actions = {
  async get (context) {
    try {
      context.commit('setFetching')
      const res = await bookPackage.get()
      context.commit('setFetchingSuccess', res)
    } catch (error) {
      context.commit('setFetchingError', error)
    }
  },

  async find (context, id) {
    try {
      context.commit('setFinding')
      const res = await bookPackage.find(id)
      context.commit('setFindingSuccess', res)
    } catch (error) {
      context.commit('setFindingError', error)
    }
  },

  async create (context, data) {
    const bookIds = data.bookIds.map(item => item.id)

    const payload = {
      title: data.title,
      description: data.description,
      photo: data.photo,
      bookIds: bookIds,
      price: data.price,
      discount: 0
    }

    try {
      context.commit('setCreating')
      const res = await bookPackage.create(payload)
      context.commit('setCreatingSuccess', res)
    } catch (error) {
      context.commit('setCreatingError', error)
    }
  },

  async edit (context, id) {
    try {
      context.commit('setEditing')
      const res = await bookPackage.find(id)
      context.commit('setEditingSuccess', res)
    } catch (error) {
      context.commit('setEditingError', error)
    }
  },

  async update (context, data) {
    const bookIds = data.bookIds.map(item => item.id)

    const payload = {
      title: data.title,
      description: data.description,
      photo: data.photo,
      bookIds: bookIds,
      price: data.price,
      discount: 0
    }

    try {
      context.commit('setUpdating')
      const res = await bookPackage.update(data.id, payload)
      context.commit('setUpdatingSuccess', res)
    } catch (error) {
      context.commit('setUpdatingError', error)
    }
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
