import auth from '@/services/auth'
import { remove } from '@/plugins/cookie'
import { ACCESS_TOKEN_KEY } from '@/shared'

const state = {
  isAuthenticated: false,
  isAuthenticatingError: null,
  isAuthenticating: false,
  loggedUser: {
    id: 0,
    email: '',
    name: '',
    roleId: 0
  }
}

const getters = {
  isAuthenticated: state => state.isAuthenticated,
  isAuthenticating: state => state.isAuthenticating,
  isAuthenticatingError: state => state.isAuthenticatingError,
  loggedUser: state => state.loggedUser
}

const mutations = {
  setLoggedUser (state, user) {
    state.loggedUser = {
      id: user.id,
      email: user.email,
      roleId: user.role.data.id,
      name: user.personal.data.fullName
    }

    state.isAuthenticated = true
  },

  logout (state) {
    state.loggedUser = {
      id: 0,
      email: '',
      name: '',
      roleId: 0
    }

    state.isAuthenticated = false
    remove(ACCESS_TOKEN_KEY)
  },

  setAuthLoading (state) {
    state.isAuthenticating = true
    state.isAuthenticatingError = null
  },

  setAuthError (state, error) {
    state.isAuthenticatingError = error
    state.isAuthenticating = false
  },

  setAuthSuccess (state) {
    state.isAuthenticating = false
    state.isAuthenticatingError = null
  }
}

const actions = {
  async auth (context, payload) {
    try {
      context.commit('setAuthLoading')
      await auth.authenticate(payload)
      context.commit('setAuthSuccess')

      const res = await auth.whoami()
      context.commit('setLoggedUser', res)
    } catch (error) {
      context.commit('setAuthError', error)
    }
  },

  async whoami (context) {
    try {
      const res = await auth.whoami()

      context.commit('setLoggedUser', res)
    } catch (error) {
      return error
    }
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
