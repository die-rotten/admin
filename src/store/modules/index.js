import auth from './auth'
import author from './author'
import book from './book'
import bookContent from './bookContent'
import bookPackage from './bookPackage'
import bookSection from './bookSection'
import publisher from './publisher'
import report from './report'
import transaction from './transaction'

export default {
  auth,
  author,
  book,
  bookContent,
  bookPackage,
  bookSection,
  publisher,
  report,
  transaction
}
