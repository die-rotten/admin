import publisher from '@/services/publisher'

const state = {
  publishers: [],
  publisher: {
    id: 0,
    name: '',
    address: '',
    phone: ''
  },

  isFetchingError: null,
  isFetching: false,

  isCreatingError: null,
  isCreating: false,

  isFindingError: null,
  isFinding: false,

  isUpdatingError: null,
  isUpdating: false,

  isDeletingError: null,
  isDeleting: false
}

const getters = {
  publishers: state => state.publishers,
  isFetching: state => state.isFetching,
  isFetchingError: state => state.isFetchingError
}

const mutations = {
  setFetchingSuccess (state, data) {
    state.publishers = data
    state.isFetching = false
    state.isFetchingError = null
  },

  setFetchingError (state, error) {
    state.isFetching = false
    state.isFetchingError = error
  },

  setFetching (state) {
    state.publishers = []
    state.isFetchingError = null
    state.isFetching = true
  },

  setFindingSuccess (state, data) {
    state.publisher = data
    state.isFinding = false
    state.isFindingError = null
  },

  setFindingError (state, error) {
    state.isFinding = false
    state.isFindingError = error
  },

  setFinding (state) {
    state.isFindingError = null
    state.isFinding = true
  },

  setCreatingSuccess (state) {
    state.isCreating = false
    state.isCreatingError = null
  },

  setCreatingError (state, error) {
    state.isCreating = false
    state.isCreatingError = error
  },

  setCreating (state) {
    state.isCreatingError = null
    state.isCreating = true
  },

  setUpdatingSuccess (state) {
    state.isUpdating = false
    state.isUpdatingError = null
  },

  setUpdatingError (state, error) {
    state.isUpdating = false
    state.isUpdatingError = error
  },

  setUpdating (state) {
    state.isUpdatingError = null
    state.isUpdating = true
  },

  setDeletingSuccess (state) {
    state.isDeleting = false
    state.isDeletingError = null
  },

  setDeletingError (state, error) {
    state.isDeleting = false
    state.isDeletingError = error
  },

  setDeleting (state) {
    state.isDeletingError = null
    state.isDeleting = true
  },

  resetPublisherForm (state) {
    state.publisher = {
      id: 0,
      name: '',
      address: '',
      phone: ''
    }
  }
}

const actions = {
  async get (context) {
    try {
      context.commit('setFetching')
      const data = await publisher.get()
      context.commit('setFetchingSuccess', data)
    } catch (error) {
      context.commit('setFetchingError', error)
    }
  },

  async find (context, id) {
    try {
      context.commit('setFinding')
      const data = await publisher.find(id)
      context.commit('setFindingSuccess', data)
    } catch (error) {
      context.commit('setFindingError', error)
    }
  },

  async update (context, payload) {
    try {
      context.commit('setUpdating')
      const data = await publisher.update(payload.id, payload)
      context.commit('setUpdatingSuccess', data)
    } catch (error) {
      context.commit('setUpdatingError', error)
    }
  },

  async create (context, payload) {
    try {
      context.commit('setCreating')
      const data = await publisher.create(payload)
      context.commit('setCreatingSuccess', data)
    } catch (error) {
      context.commit('setCreatingError', error)
    }
  },

  async delete (context, id) {
    try {
      context.commit('setDeleting')
      const data = await publisher.delete(id)
      context.commit('setDeletingSuccess', data)
    } catch (error) {
      context.commit('setDeletingError', error)
    }
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
