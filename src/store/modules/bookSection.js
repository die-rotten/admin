import bookSection from '@/services/bookSection'
import book from '@/services/book'

const state = {
  isCreating: false,
  isCreatingError: null,

  isEditing: false,
  isEditingError: null,

  isUpdating: false,
  isUpdatingError: null,

  sections: [],

  isFetching: false,
  isFetchingError: null,

  form: {
    id: 0,
    bookId: 0,
    title: '',
    description: ''
  }
}

const mutations = {
  setFetching (state) {
    state.isFetchingError = null
    state.isFetching = true
  },

  setFetchingSuccess (state, data) {
    state.sections = data
    state.isFetching = false
  },

  setFetchingError (state, error) {
    state.isFetchingError = error
    state.isFetching = false
  },

  setCreating (state) {
    state.isCreating = true
    state.isCreatingError = null
  },

  setCreatingSuccess (state) {
    state.isCreating = false
  },

  setCreatingError (state, error) {
    state.isCreatingError = error
    state.isCreating = false
  },

  setUpdating (state) {
    state.isUpdating = true
    state.isUpdatingError = null
  },

  setUpdatingSuccess (state) {
    state.isUpdating = false
  },

  setUpdatingError (state, error) {
    state.isUpdatingError = error
    state.isUpdating = false
  },

  setEditing (state) {
    state.isEditing = true
    state.isEditingError = null
  },

  setEditingSuccess (state) {
    state.isEditing = false
  },

  setEditingError (state, error) {
    state.isEditingError = error
    state.isEditing = false
  },

  setForm (state, data) {
    state.form = {
      ...state.form,
      ...data
    }
  },

  resetForm (state) {
    state.form = {
      id: 0,
      bookId: 0,
      title: '',
      description: ''
    }
  }
}

const actions = {
  async get (context, id) {
    try {
      context.commit('setFetching')
      const res = await book.getSections(id)
      context.commit('setFetchingSuccess', res)
    } catch (error) {
      context.commit('setFetchingError', error)
    }
  },

  async create (context, payload) {
    try {
      context.commit('setCreating')
      await bookSection.create(payload)
      context.commit('setCreatingSuccess')
    } catch (error) {
      context.commit('setCreatingError', error)
    }
  },

  async edit (context, id) {
    try {
      context.commit('setEditing')
      const data = await bookSection.find(id)
      context.commit('setForm', data)
      context.commit('setEditingSuccess')
    } catch (error) {
      context.commit('setEditingError', error)
    }
  },

  async update (context, data) {
    const payload = {
      bookId: data.bookId,
      title: data.title,
      description: data.description
    }

    try {
      context.commit('setUpdating')
      await bookSection.update(data.id, payload)
      context.commit('setUpdatingSuccess')
    } catch (error) {
      context.commit('setUpdatingError', error)
    }
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
