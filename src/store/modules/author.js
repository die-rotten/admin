import author from '@/services/author'

const state = {
  authors: [],
  author: {
    id: 0,
    title: '',
    firstName: '',
    lastName: '',
    email: '',
    phone: '',
    status: true,
    publisherId: 0,
    publisher: {
      data: {
        id: 0
      }
    }
  },

  form: {
    id: 0,
    title: '',
    firstName: '',
    lastName: '',
    email: '',
    phone: '',
    status: true,
    publisherId: 0
  },

  isFetchingError: null,
  isFetching: false,

  isFindingError: null,
  isFinding: false,

  isCreatingError: null,
  isCreating: false,

  isUpdatingError: null,
  isUpdating: false,

  isDeletingError: null,
  isDeleting: false
}

const getters = {
  authors: state => state.authors,
  isFetching: state => state.isFetching,
  isFetchingError: state => state.isFetchingError
}

const mutations = {
  setFetchingSuccess (state, data) {
    state.authors = data
    state.isFetching = false
    state.isFetchingError = null
  },

  setFetchingError (state, error) {
    state.isFetching = false
    state.isFetchingError = error
  },

  setFetching (state) {
    state.authors = []
    state.isFetchingError = null
    state.isFetching = true
  },

  setFindingSuccess (state, data) {
    state.author = {
      ...data
    }
    state.isFinding = false
    state.isFindingError = null
  },

  setFindingError (state, error) {
    state.isFinding = false
    state.isFindingError = error
  },

  setFinding (state) {
    state.isFindingError = null
    state.isFinding = true
  },

  setCreating (state) {
    state.isCreatingError = null
    state.isCreating = true
  },

  setCreatingSuccess (state) {
    state.isCreatingError = null
    state.isCreating = false
  },

  setCreatingError (state, error) {
    state.isCreatingError = error
    state.isCreating = false
  },

  setAuthorForm (state) {
    state.form = {
      ...state.author,
      publisherId: state.author.publisher.data.id
    }
  },

  resetAuthorForm (state) {
    state.form = {
      id: 0,
      title: '',
      firstName: '',
      lastName: '',
      email: '',
      phone: '',
      status: true,
      publisherId: 0
    }
  },

  setUpdating (state) {
    state.isUpdatingError = null
    state.isUpdating = true
  },

  setUpdatingSuccess (state) {
    state.isUpdatingError = null
    state.isUpdating = false
  },

  setUpdatingError (state, error) {
    state.isUpdatingError = error
    state.isUpdating = false
  },

  setDeleting (state) {
    state.isDeletingError = null
    state.isDeleting = true
  },

  setDeletingSuccess (state) {
    state.isDeletingError = null
    state.isDeleting = false
  },

  setDeletingError (state, error) {
    state.isDeletingError = error
    state.isDeleting = false
  }
}

const actions = {
  async get (context) {
    try {
      context.commit('setFetching')
      const data = await author.get()
      context.commit('setFetchingSuccess', data)
    } catch (error) {
      context.commit('setFetchingError', error)
    }
  },

  async find (context, id) {
    try {
      context.commit('setFinding')
      const data = await author.find(id)
      context.commit('setFindingSuccess', data)
    } catch (error) {
      context.commit('setFindingError', error)
    }
  },

  async create (context, data) {
    const payload = {
      publisherId: data.publisherId,
      email: data.email,
      firstName: data.firstName,
      lastName: data.lastName,
      phone: data.phone,
      title: data.title
    }

    try {
      context.commit('setCreating')
      await author.create(payload)
      context.commit('setCreatingSuccess')
    } catch (error) {
      context.commit('setCreatingError', error)
    }
  },

  async update (context, data) {
    const payload = {
      publisherId: data.publisherId,
      email: data.email,
      firstName: data.firstName,
      lastName: data.lastName,
      phone: data.phone,
      title: data.title
    }

    try {
      context.commit('setUpdating')
      await author.update(data.id, payload)
      context.commit('setUpdatingSuccess')
    } catch (error) {
      context.commit('setUpdatingError', error)
    }
  },

  async delete (context, id) {
    try {
      context.commit('setDeleting')
      const data = await author.delete(id)
      context.commit('setDeletingSuccess', data)
    } catch (error) {
      context.commit('setDeletingError', error)
    }
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
