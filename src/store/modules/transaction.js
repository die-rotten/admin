import transaction from '@/services/transaction'

const state = {
  transactions: [],
  transaction: {},
  payment: {},

  isFetching: false,
  isFetchingError: null,

  isFinding: false,
  isFindingError: null,

  isFindingPayment: false,
  isFindingPaymentError: null,

  isApprovePayment: false,
  isApprovePaymentError: null
}

const mutations = {
  setFetching (state) {
    state.isFetching = true
    state.isFetchingError = null
  },

  setFetchingSuccess (state, data) {
    state.transactions = data
    state.isFetching = false
  },

  setFetchingError (state, error) {
    state.isFetchingError = error
    state.isFetching = false
  },

  setFinding (state) {
    state.isFinding = true
    state.isFindingError = null
  },

  setFindingSuccess (state, data) {
    state.transaction = data
    state.isFinding = false
  },

  setFindingError (state, error) {
    state.isFindingError = error
    state.isFinding = false
  },

  setFindingPayment (state) {
    state.isFindingPayment = true
    state.isFindingPaymentError = null
  },

  setFindingPaymentSuccess (state, data) {
    state.payment = data
    state.isFindingPayment = false
  },

  setFindingPaymentError (state, error) {
    state.isFindingPaymentError = error
    state.isFindingPayment = false
  },

  setApprovePayment (state) {
    state.isApprovePayment = true
    state.isApprovePaymentError = null
  },

  setApprovePaymentSuccess (state) {
    state.isApprovePayment = false
  },

  setApprovePaymentError (state, error) {
    state.isApprovePaymentError = error
    state.isApprovePayment = false
  }
}

const actions = {
  async get (context) {
    try {
      context.commit('setFetching')
      const res = await transaction.get()
      context.commit('setFetchingSuccess', res)
    } catch (error) {
      context.commit('setFetchingError', error)
    }
  },

  async find (context, id) {
    try {
      context.commit('setFinding')
      const res = await transaction.find(id)
      context.commit('setFindingSuccess', res)
    } catch (error) {
      context.commit('setFindingError', error)
    }
  },

  async findPayment (context, id) {
    try {
      context.commit('setFindingPayment')
      const res = await transaction.findPayment(id)
      context.commit('setFindingPaymentSuccess', res)
    } catch (error) {
      context.commit('setFindingPaymentError', error)
    }
  },

  async approvePayment (context, id) {
    try {
      context.commit('setApprovePayment')
      await transaction.approvePayment(id)
      context.commit('setApprovePaymentSuccess')
    } catch (error) {
      context.commit('setApprovePaymentError', error)
    }
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
