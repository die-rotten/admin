import bookContent from '@/services/bookContent'

const state = {
  isCreating: false,
  isCreatingError: null,

  isEditing: false,
  isEditingError: null,

  isUpdating: false,
  isUpdatingError: null,

  form: {
    id: 0,
    sectionId: null,
    title: '',
    content: '',
    duration: null,
    durationUnit: ''
  }
}

const mutations = {
  setCreating (state) {
    state.isCreating = true
    state.isCreatingError = null
  },

  setCreatingError (state, error) {
    state.isCreatingError = error
    state.isCreating = false
  },

  setCreatingSuccess (state) {
    state.isCreating = false
  },

  setEditing (state) {
    state.isEditing = true
    state.isEditingError = null
  },

  setEditingSuccess (state, data) {
    const content = JSON.parse(data.content).content

    state.form = {
      ...data,
      content: content
    }
    state.isEditing = false
  },

  setEditingError (state, error) {
    state.isEditingError = error
    state.isEditing = false
  },

  setUpdating (state) {
    state.isUpdating = true
    state.isUpdatingError = null
  },

  setUpdatingError (state, error) {
    state.isUpdatingError = error
    state.isUpdating = false
  },

  setUpdatingSuccess (state) {
    state.isUpdating = false
  },

  resetForm (state) {
    state.form = {
      id: 0,
      sectionId: 0,
      title: '',
      content: '',
      duration: 0,
      durationUnit: ''
    }
  }
}

const actions = {
  async create (context, data) {
    const payload = {
      sectionId: data.sectionId,
      title: data.title,
      content: data.content,
      type: 'Bacaan',
      duration: data.duration,
      durationUnit: data.durationUnit
    }

    try {
      context.commit('setCreating')
      await bookContent.create(payload)
      context.commit('setCreatingSuccess')
    } catch (error) {
      context.commit('setCreatingError', error)
    }
  },

  async edit (context, id) {
    try {
      context.commit('setEditing')
      const res = await bookContent.find(id)
      context.commit('setEditingSuccess', res)
    } catch (error) {
      context.commit('setEditingError', error)
    }
  },

  async update (context, data) {
    const payload = {
      sectionId: data.sectionId,
      title: data.title,
      content: data.content,
      type: 'Text',
      duration: data.duration,
      durationUnit: data.durationUnit
    }

    try {
      context.commit('setUpdating')
      await bookContent.update(data.id, payload)
      context.commit('setUpdatingSuccess')
    } catch (error) {
      context.commit('setUpdatingError', error)
    }
  }
}

export default {
  namespaced: true,
  state,
  actions,
  mutations
}
