import report from '@/services/report'

const state = {
  dashboardReport: {
    authorAmount: 0,
    transactionAmount: 0,
    userAmount: 0,
    packageAmount: 0
  },

  popularPackages: [],
  transactionsAmount: {},

  isFetchingReport: false,
  isFetchingReportError: null,

  isFetchingPopularPackagesReport: false,
  isFetchingPopularPackagesReportError: null,

  isFetchingTransactionAmountReport: false,
  isFetchingTransactionAmountReportError: null
}

const mutations = {
  setFetchingReport (state) {
    state.isFetchingReport = true
    state.isFetchingReportError = null
  },

  setFetchingReportSuccess (state, data) {
    state.dashboardReport = data
    state.isFetchingReport = false
  },

  setFetchingReportError (state, error) {
    state.isFetchingReportError = error
    state.isFetchingReport = false
  },

  setFetchingPopularPackagesReport (state) {
    state.isFetchingPopularPackagesReport = true
    state.isFetchingPopularPackagesReportError = null
  },

  setFetchingPopularPackagesReportSuccess (state, data) {
    state.popularPackages = data
    state.isFetchingPopularPackagesReport = false
  },

  setFetchingPopularPackagesReportError (state, error) {
    state.isFetchingPopularPackagesReportError = error
    state.isFetchingPopularPackagesReport = false
  },

  setFetchingTransactionAmountReport (state) {
    state.isFetchingTransactionAmountReport = true
    state.isFetchingTransactionAmountReportError = null
  },

  setFetchingTransactionAmountReportSuccess (state, data) {
    state.transactionsAmount = data
    state.isFetchingTransactionAmountReport = false
  },

  setFetchingTransactionAmountReportError (state, error) {
    state.isFetchingTransactionAmountReportError = error
    state.isFetchingTransactionAmountReport = false
  }
}

const actions = {
  async getDashboardReport ({ commit }) {
    try {
      commit('setFetchingReport')
      const author = await report.getAuthorsAmount()
      const packages = await report.getPackagesAmount()
      const transactions = await report.getTransactionsQuantity()
      const users = await report.getUsersAmount()

      const res = {
        authorAmount: author.amount,
        packageAmount: packages.amount,
        transactionAmount: transactions.amount,
        userAmount: users.amount
      }

      commit('setFetchingReportSuccess', res)
    } catch (error) {
      commit('setFetchingReportError', error)
    }
  },

  async getPopularPackages ({ commit }) {
    try {
      commit('setFetchingPopularPackagesReport')
      const res = await report.getPopularPackages()
      commit('setFetchingPopularPackagesReportSuccess', res)
    } catch (error) {
      commit('setFetchingPopularPackagesReportError', error)
    }
  },

  async getTransactionsAmount ({ commit }) {
    try {
      commit('setFetchingTransactionAmountReport')
      const res = await report.getTransactionsAmount()
      commit('setFetchingTransactionAmountReportSuccess', res)
    } catch (error) {
      commit('setFetchingTransactionAmountReportError', error)
    }
  }
}

export default {
  namespaced: true,
  state,
  actions,
  mutations
}
